# Modelagem Programação Linear Inteira

Projeto de modelagem de um problema através de *Programação Linear Inteira* para a disciplina de Pesquisa Operacional - UFPB.

Utilizando o solver presente no [OR-Tools](https://developers.google.com/optimization/mip/integer_opt).

## Execução do Programa 

```shell
python main.py data.txt
```
## Formato do arquivo de entrada

```shell
200 100 440 250 220 350 180 250   #demandas semanais de P1
0 0 0 0                           #estoque inicial (P1 P2 C1 C2)
400                               #custo de compra de C1
350                               #custo de compra de C2
120000                            #custo fixo de compra
450                               #custo semanal para estocar P1
180                               #custo semanal para estocar P2
50                                #custo semanal para estocar C1
100                               #custo semanal para estocar C2
```
