# File Name:        main.py
# Author:           Joseilton Ferreira
# Created:          05-dez-2020
# Last Modified:    14-dez-2020
# Python Version:   3.7.1

import sys
import os
from ortools.linear_solver import pywraplp
from tabulate import tabulate

def clear():
    if os.name == 'nt':
        os.system('cls')
    else:
        os.system('clear')
    return

# dados de entrada
with open(sys.argv[1], 'r') as file:
    # demanda
    d = [int(i) for i in file.readline().split(' ')]
    # estoque incial p1, p2, c1, c2
    e_init = [int(i) for i in file.readline().split(' ')]
    # custos material [c_c1 c_c2] / custo fixo [cf] / custo estoque [c_ep1 c_ep2 c_ec1 c_ec2]
    c_c1 = int(file.readline())
    c_c2 = int(file.readline())
    c_cf = int(file.readline())
    c_ep1 = int(file.readline())
    c_ep2 = int(file.readline())
    c_ec1 = int(file.readline())
    c_ec2 = int(file.readline())

# limite de tempo de execucao
time_limit = 1000000

# periodo
period = len(d)

# configuracoes solver
solver = pywraplp.Solver.CreateSolver('SCIP')
infinity = solver.infinity()
solver.SetTimeLimit(time_limit)

# array de variaveis {var}_t
p1 = []
p2 = []
c1 = []
c2 = []
cf = []
ep1 = [e_init[0]]
ep2 = [e_init[1]]
ec1 = [e_init[2]]
ec2 = [e_init[3]]

# big m para garantir que quando houver comprar cf_t = 1
big_m = 9999999

# add variaveis aos arrays
for i in range(period):
    p1.append(solver.IntVar(0.0, infinity, f'p1_{i + 1}'))
    p2.append(solver.IntVar(0.0, infinity, f'p2_{i + 1}'))
    c1.append(solver.IntVar(0.0, infinity, f'c1_{i + 1}'))
    c2.append(solver.IntVar(0.0, infinity, f'c2_{i + 1}'))
    cf.append(solver.IntVar(0.0, 1.0, f'cf_{i + 1}'))
    ep1.append(solver.IntVar(0.0, infinity, f'ep1_{i + 1}'))
    ep2.append(solver.IntVar(0.0, infinity, f'ep2_{i + 1}'))
    ec1.append(solver.IntVar(0.0, infinity, f'ec1_{i + 1}'))
    ec2.append(solver.IntVar(0.0, infinity, f'ec2_{i + 1}'))

# add restricoes ao solver
for i in range(period):
    solver.Add(p1[i] >= d[i] - ep1[i])
    solver.Add(2 * p1[i] <= p2[i] + ep2[i])
    solver.Add(2 * p2[i] <= c2[i] + ec2[i])
    solver.Add((3 * p1[i]) + p2[i] <= c1[i] + ec1[i])
    solver.Add(ep1[i + 1] == p1[i] + ep1[i] - d[i])
    solver.Add(ep2[i + 1] == p2[i] + ep2[i] - (2 * p1[i]))
    solver.Add(ec2[i + 1] == c2[i] + ec2[i] - (2 * p2[i]))
    solver.Add(ec1[i + 1] == c1[i] + ec1[i] - (3 * p1[i]) - p2[i])
    solver.Add(big_m * cf[i] >= c1[i] + c2[i])

# funcao objetivo
objective = solver.Objective()
objective.SetMinimization()

for i in range(period):
    objective.SetCoefficient(c1[i], c_c1)
    objective.SetCoefficient(c2[i], c_c2)
    objective.SetCoefficient(cf[i], c_cf)
    objective.SetCoefficient(ep1[i + 1], c_ep1)
    objective.SetCoefficient(ep2[i + 1], c_ep2)
    objective.SetCoefficient(ec1[i + 1], c_ec1)
    objective.SetCoefficient(ec2[i + 1], c_ec2)

# solucao
status = solver.Solve()
if status == pywraplp.Solver.OPTIMAL:
    clear()
    print('\n\n')
    data = []
    ix = []
    hd = ['Qtd P1\nProd.', 'Qtd P2\nProd.', 'Qtd C1\nComp.','Qtd C2\nComp.', 'Qtd P1\nEst.', 'Custo P1\nEst. (R$)',
          'Qtd P2\nEst.', 'Custo P2\nEst. (R$)', 'Qtd C1\nEst.', 'Custo C1\nEst. (R$)', 'Qtd C2\nEst.',
          'Custo C2\nEst. (R$)']
    for i in range(period):
        arr = [p1[i].solution_value(), p2[i].solution_value(), c1[i].solution_value(), c2[i].solution_value(),
               ep1[i + 1].solution_value(), ep1[i + 1].solution_value() * c_ep1, ep2[i + 1].solution_value(),
               ep2[i + 1].solution_value() * c_ep2, ec1[i + 1].solution_value(),
               ec1[i + 1].solution_value() * c_ec1,ec2[i + 1].solution_value(), ec2[i + 1].solution_value() * c_ec2]
        ix.append(f'Semana {i + 1}')
        data.append(arr)
    print(tabulate(data, headers=hd, showindex=ix, tablefmt='fancy_grid'))
    print('\n* Prod. : Produzidos\n* Comp. : Comprados\n* Est. : Estocados')
else:
    print('Error in solution')
